import { IStyle } from 'fela'

const footerStyle: IStyle = {
  marginTop: 'auto',
  padding: '15px',
  textAlign: 'right',
  fontStyle: 'italic',
}

export default footerStyle
