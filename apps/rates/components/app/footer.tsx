import { Box } from '@workday-homework/components'
import footerStyle from './footer.style'

const Footer = () => (
  <Box style={footerStyle}>
    <Box
      as="hr"
      style={{
        marginBottom: '30px',
      }}
    />
    <Box as="span">&copy; Andrei 2021</Box>
  </Box>
)

export default Footer
