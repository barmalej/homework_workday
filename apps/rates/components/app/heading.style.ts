import { IStyle } from 'fela'

const headingStyle: Record<'wrapper' | 'heading', IStyle> = {
  wrapper: { padding: '15px', textAlign: 'right' },
  heading: { fontStyle: 'italic' },
}

export default headingStyle
