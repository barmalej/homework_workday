import { Box } from '@workday-homework/components'
import headingStyle from './heading.style'

const Heading = () => (
  <Box as="header" style={headingStyle.wrapper}>
    <Box as="h1" style={headingStyle.heading}>
      Latest currency rates
    </Box>
  </Box>
)

export default Heading
