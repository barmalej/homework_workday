import { Box, Col, Row } from '@workday-homework/components'
import rateStyle from '../index/rate.style'

const LoaderTiles = () => (
  <>
    {[...new Array(33)].map((_, key) => (
      <Col
        key={key}
        xs={4}
        style={{
          marginTop: '10px',
        }}
      >
        <Row key={key} style={rateStyle}>
          <Col xs={12}>
            <Box
              className="line-shine-15"
              style={{
                width: '100%',
              }}
            />
          </Col>
          <Col
            xs={7}
            xsStart={6}
            style={{
              paddingTop: '10px',
              textAlign: 'right',
            }}
          >
            <Box
              className="line-shine-10"
              style={{
                width: '100%',
              }}
            />
          </Col>
        </Row>
      </Col>
    ))}
  </>
)

export default LoaderTiles
