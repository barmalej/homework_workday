import { Box, Col, Row } from '@workday-homework/components'
import {
  colors,
  currenciesOptions,
  opacity,
  shiftColor,
} from '@workday-homework/utils'
import { useDispatch, useSelector } from 'react-redux'
import Select from 'react-select'
import { setBaseCurrency, setSimulateError } from '../../lib/redux/actions'
import { AppState, SelectOption } from '../../lib/types'
import FetchButton from './fetch-button'

const ControlBar = () => {
  const dispatch = useDispatch()
  const baseCurrency = useSelector((state: AppState) => state.baseCurrency)
  const simulateError = useSelector((state: AppState) => state.simulateError)
  const onSelectChange = (option: SelectOption) => {
    dispatch(setBaseCurrency(option))
  }
  const onChange = () => {
    dispatch(setSimulateError(!simulateError))
  }

  return (
    <Row>
      <Col xs={4}>
        <Box as="label" style={{ cursor: 'pointer' }}>
          <Box
            as="input"
            type="checkbox"
            value={simulateError}
            style={{ marginRight: '10px' }}
            onChange={onChange}
          />
          Simulate ERROR
        </Box>
      </Col>
      <Col xs={4}>
        <Select
          instanceId="base-currency-select"
          options={currenciesOptions}
          defaultValue={currenciesOptions[0]}
          value={baseCurrency}
          isSearchable
          isClearable
          onChange={onSelectChange}
          styles={{
            container: (provided) => ({
              ...provided,
              backgroundColor: colors.pickledBluewood,
              width: '100%',
            }),
            control: (provided) => ({
              ...provided,
              backgroundColor: colors.pickledBluewood,
              borderRadius: 0,
              color: 'white',
              padding: '10px',
              boxSizing: 'border-box',
              width: '100%',
            }),
            placeholder: (provided) => ({ ...provided, color: 'white' }),
            valueContainer: (provided) => ({ ...provided, color: 'white' }),
            singleValue: (provided) => ({ ...provided, color: 'white' }),
            input: (provided) => ({ ...provided, color: 'white' }),
            menu: (provided) => ({
              ...provided,
              backgroundColor: opacity(colors.pickledBluewood, 0.5),
              borderRadius: 0,
            }),
            menuList: (provided) => ({
              ...provided,
              backgroundColor: opacity(colors.pickledBluewood, 0.5),
            }),
            option: (provided, state) => ({
              ...provided,
              color: state.isSelected ? colors.silver : 'white',
              backgroundColor:
                state.isFocused || state.isSelected
                  ? shiftColor(colors.shattleGrey, -5)
                  : colors.shattleGrey,
            }),
          }}
        />
      </Col>
      <Col xs={4}>
        <FetchButton />
      </Col>
    </Row>
  )
}

export default ControlBar
