import { Button } from '@workday-homework/components'
import { useDispatch } from 'react-redux'
import { fetchRates } from '../../lib/redux/actions'

const FetchButton = () => {
  const dispatch = useDispatch()
  const onClick = () => dispatch(fetchRates())

  return (
    <Button primary onClick={onClick}>
      Fetch
    </Button>
  )
}

export default FetchButton
