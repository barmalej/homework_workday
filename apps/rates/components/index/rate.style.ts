import { IStyle } from 'fela'
import { shiftColor, colors } from '@workday-homework/utils'

const rateStyle: IStyle = {
  cursor: 'pointer',
  padding: '10px',
  // margin: '5px',
  boxSizing: 'border-box',
  backgroundColor: colors.shattleGrey,
  ':hover': {
    backgroundColor: shiftColor(colors.shattleGrey, -15),
  },
}

export default rateStyle
