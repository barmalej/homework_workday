import { Col, Row } from '@workday-homework/components'
import { colors } from '@workday-homework/utils'
import rateStyle from './rate.style'

const Rate = ({
  label,
  value,
  symbol,
}: {
  label: string
  value: number
  symbol: string
}) => (
  <Row style={rateStyle}>
    <Col xs={12}>
      <Row>
        <Col xs={6}>
          <h3>{symbol}</h3>
        </Col>
        <Col xs={6} style={{ textAlign: 'right' }}>
          <h3>{label}</h3>
        </Col>
      </Row>
    </Col>
    <Col
      xs={12}
      style={{ paddingTop: '10px', color: colors.silver, textAlign: 'right' }}
    >
      {value}
    </Col>
  </Row>
)

export default Rate
