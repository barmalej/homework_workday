import { Col, Row } from '@workday-homework/components'
import getSymbol from 'currency-symbol-map'
import { useSelector } from 'react-redux'
import { AppState } from '../../lib/types'
import LoaderTiles from '../app/loader-tiles'
import Rate from './rate'
import ratesStyle from './rates.style'

const Rates = () => {
  const rates = useSelector((state: AppState) => state.rates)
  const loading = useSelector((state: AppState) => state.loading)

  return (
    <Row style={ratesStyle}>
      {rates &&
        Object.keys(rates.rates).map((key) => (
          <Col
            key={key}
            xs={4}
            style={{
              marginTop: '10px',
            }}
          >
            <Rate
              label={key}
              symbol={getSymbol(key)}
              value={rates.rates[key]}
            />
          </Col>
        ))}
      {!rates && loading && <LoaderTiles />}
    </Row>
  )
}

export default Rates
