import { ActionTypes, Rates, SelectOption } from '../types'

export const fetchRates = () => ({ type: ActionTypes.FETCH_RATES })

export type FetchRatesAction = ReturnType<typeof fetchRates>

export const fetchRatesComplete = (rates: Rates) => ({
  type: ActionTypes.FETCH_RATES_COMPLETE,
  payload: { rates },
})

export type FetchRatesCompleteAction = ReturnType<typeof fetchRatesComplete>

export const setBaseCurrency = (baseCurrency: SelectOption) => ({
  type: ActionTypes.SET_BASE_CURRENCY,
  payload: { baseCurrency },
})

export type SetBaseCurrencyAction = ReturnType<typeof setBaseCurrency>

export const failedToFetch = (error: Error) => ({
  type: ActionTypes.FAILED_TO_FETCH,
  payload: { error },
})

export type FailedToFetchAction = ReturnType<typeof failedToFetch>

export const setSimulateError = (simulateError: boolean) => ({
  type: ActionTypes.SET_SIMULATE_ERROR,
  payload: { simulateError },
})

export type SetSimulateErrorAction = ReturnType<typeof setSimulateError>
