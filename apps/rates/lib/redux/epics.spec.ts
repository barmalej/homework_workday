import {
  fetchRatesPromise,
  TEST_BASE_CURRENCY,
  TEST_ERROR,
  TEST_RATES,
} from '@workday-homework/utils'
import { StateObservable } from 'redux-observable'
import { of } from 'rxjs'
import { ActionTypes, AppState } from '../types'
import { fetchRates } from './actions'
import { fetchRatesEpic } from './epics'

const state$ = of({
  baseCurrency: TEST_BASE_CURRENCY,
}) as StateObservable<AppState>
jest.mock('@workday-homework/utils')

test.each([
  [
    'successful',
    1,
    TEST_BASE_CURRENCY.value,
    ActionTypes.FETCH_RATES_COMPLETE,
    'rates',
    TEST_RATES,
    () => Promise.resolve(TEST_RATES),
  ],
  [
    'with error',
    1,
    TEST_BASE_CURRENCY.value,
    ActionTypes.FAILED_TO_FETCH,
    'error',
    { message: TEST_ERROR },
    () => Promise.reject(new Error(TEST_ERROR)),
  ],
])(
  'fetch rates epic %s',
  (
    _label,
    times,
    baseCurrency,
    actionType,
    payloadPath,
    testValue,
    mockImplementation
  ) => {
    fetchRatesPromise['mockImplementationOnce'](mockImplementation)

    fetchRatesEpic(of(fetchRates()), state$).subscribe((result) => {
      expect(fetchRatesPromise).toHaveBeenCalledTimes(times)
      expect(fetchRatesPromise).toHaveBeenCalledWith(baseCurrency)
      expect(result.type).toBe(actionType)
      expect(result.payload[payloadPath]).toBe(testValue)
    })
  }
)
