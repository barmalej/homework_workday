import { fetchRatesPromise } from '@workday-homework/utils'
import { ofType, StateObservable } from 'redux-observable'
import {
  catchError,
  delay,
  from,
  map,
  mergeMap,
  Observable,
  of,
  withLatestFrom,
} from 'rxjs'
import { ActionTypes, AppState } from '../types'
import { failedToFetch, FetchRatesAction, fetchRatesComplete } from './actions'

export const fetchRatesEpic = (
  action$: Observable<FetchRatesAction>,
  state$: StateObservable<AppState>
) =>
  action$.pipe(
    ofType(ActionTypes.FETCH_RATES),
    withLatestFrom(state$),
    mergeMap(([_, state]) => {
      const baseCurrency = state.baseCurrency
      const promise = state.simulateError
        ? Promise.reject(new Error('This is a test error'))
        : fetchRatesPromise(baseCurrency?.value)

      return from(promise).pipe(
        // delay in order to demonstrate the loader tiles
        delay(500),
        map((rates) => fetchRatesComplete(rates)),
        catchError((err) => of(failedToFetch(err)))
      )
    })
  )
