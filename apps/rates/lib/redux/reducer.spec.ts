import {
  failedToFetch,
  fetchRates,
  fetchRatesComplete,
  setBaseCurrency,
} from './actions'
import {
  TEST_BASE_CURRENCY,
  TEST_ERROR,
  TEST_RATES,
} from '@workday-homework/utils'
import { reducer } from './reducer'
import { initialState } from './store'
import { AppState } from '../types'

let state: AppState

describe('reducer', () => {
  beforeEach(() => {
    state = initialState
  })

  it('fetch rates', () => {
    expect(reducer(state, fetchRates()).loading).toBeTruthy()
  })

  it('fetch rates complete', () => {
    const newState = reducer(state, fetchRatesComplete(TEST_RATES))

    expect(newState.loading).toBeFalsy()
    expect(newState.rates).toBe(TEST_RATES)
  })

  it('set base currency', () => {
    expect(
      reducer(state, setBaseCurrency(TEST_BASE_CURRENCY)).baseCurrency
    ).toBe(TEST_BASE_CURRENCY)
  })

  it('failed to fetch', () => {
    expect(
      reducer(state, failedToFetch(new Error(TEST_ERROR))).error.message
    ).toBe(TEST_ERROR)
  })

  it('wrong action', () => {
    expect(reducer(state, { type: 'WRONG_ACTION' })).toBe(initialState)
  })
})
