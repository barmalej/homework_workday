import { Action } from 'redux'
import produce from 'immer'
import {
  FailedToFetchAction,
  FetchRatesCompleteAction,
  SetBaseCurrencyAction,
  SetSimulateErrorAction,
} from './actions'
import { initialState } from './store'
import { WritableDraft } from 'immer/dist/internal'
import { ActionTypes, AppState } from '../types'

const reducers: (
  draft: WritableDraft<AppState>
) => Record<ActionTypes, (action: Action) => void> = (draft) => ({
  [ActionTypes.FETCH_RATES_COMPLETE]: (action: FetchRatesCompleteAction) => {
    draft.rates = action.payload.rates
    draft.loading = false
  },
  [ActionTypes.FETCH_RATES]: () => {
    draft.rates = undefined
    draft.loading = true
  },
  [ActionTypes.SET_BASE_CURRENCY]: (action: SetBaseCurrencyAction) => {
    draft.baseCurrency = action.payload.baseCurrency
  },
  [ActionTypes.FAILED_TO_FETCH]: (action: FailedToFetchAction) => {
    draft.error = action.payload.error
    draft.loading = false
    draft.simulateError = false
  },
  [ActionTypes.SET_SIMULATE_ERROR]: (action: SetSimulateErrorAction) => {
    draft.simulateError = action.payload.simulateError
  },
})

export const reducer = (state = initialState, action: Action) => {
  try {
    return produce(state, (draft) => reducers(draft)[action.type](action))
  } catch (e) {
    return state
  }
}
