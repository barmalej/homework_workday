import { combineEpics } from 'redux-observable'
import { fetchRatesEpic } from './epics'

const rootEpic = combineEpics(fetchRatesEpic)

export default rootEpic
