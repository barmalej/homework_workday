import produce from 'immer'
import { useMemo } from 'react'
import { createStore, applyMiddleware, Store } from 'redux'
import { composeWithDevTools } from 'redux-devtools-extension'
import { createEpicMiddleware } from 'redux-observable'
import { reducer } from '.'
import { AppState } from '../types'
import rootEpic from './root-epic'

let store: Store

export const initialState: AppState = produce(
  { loading: false, simulateError: false },
  (d) => d
)

function initStore(preloadedState = initialState) {
  const epicsMiddleware = createEpicMiddleware()
  const store = createStore(
    reducer,
    preloadedState,
    composeWithDevTools(applyMiddleware(epicsMiddleware))
  )
  epicsMiddleware.run(rootEpic as any)

  return store
}

export const initializeStore = (preloadedState: AppState) => {
  let _store = store ?? initStore(preloadedState)

  // After navigating to a page with an initial Redux state, merge that state
  // with the current state in the store, and create a new store
  if (preloadedState && store) {
    _store = initStore({
      ...store.getState(),
      ...preloadedState,
    })
    // Reset the current store
    store = undefined
  }

  // For SSG and SSR always create a new store
  if (typeof window === 'undefined') return _store
  // Create the store once in the client
  if (!store) store = _store

  return _store
}

export function useStore(initialState: AppState) {
  const store = useMemo(() => initializeStore(initialState), [initialState])
  return store
}
