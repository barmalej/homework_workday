export enum ActionTypes {
  FETCH_RATES = 'Fetch rates',
  FETCH_RATES_COMPLETE = 'Fetch rates complete',
  SET_BASE_CURRENCY = 'Set base currency',
  FAILED_TO_FETCH = 'Failed to fetch',
  SET_SIMULATE_ERROR = 'Set simulate error',
}

export interface Rates {
  date: string
  base: string
  rates: Record<string, number>
}

export interface SelectOption {
  label: string
  value: string
}

export interface AppState {
  rates?: Rates
  loading: boolean
  baseCurrency?: SelectOption
  error?: Error
  simulateError: boolean
}
