import { Box, Button } from '@workday-homework/components'
import { useRouter } from 'next/router'

const Error = () => {
  const router = useRouter()

  return (
    <Box
      style={{
        height: '100%',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'column',
      }}
    >
      <Box as="h1" style={{ textAlign: 'center' }}>
        The page you requested
        <br />
        was not found!
      </Box>
      <Box style={{ width: '100px', height: '50px' }}>
        <Button
          style={{ marginTop: '50px' }}
          onClick={() => {
            router.push('/')
          }}
        >
          Go back
        </Button>
      </Box>
    </Box>
  )
}

export default Error
