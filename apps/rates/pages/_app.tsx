import { Box, PageContainer } from '@workday-homework/components'
import FelaProvider from '@workday-homework/fela/fela-provider'
import { Provider } from 'react-redux'
import Footer from '../components/app/footer'
import Heading from '../components/app/heading'
import { useStore } from '../lib/redux'

export default function App({ Component, pageProps, renderer }) {
  const store = useStore(pageProps.initialReduxState)

  return (
    <FelaProvider renderer={renderer}>
      <Provider store={store}>
        <PageContainer>
          <Heading />
          <Box>
            <Box as="hr" style={{ marginBottom: '15px' }} />
          </Box>
          <Component {...pageProps} />
          <Footer />
        </PageContainer>
      </Provider>
    </FelaProvider>
  )
}
