import Document, {
  Html,
  Head,
  Main,
  NextScript,
  DocumentContext,
} from 'next/document'
import { JSXElementConstructor, ReactElement } from 'react'
import { renderToNodeList } from 'react-fela'
import getFelaRenderer from '@workday-homework/fela/get-fela-renderer'

export default class MyDocument extends Document {
  static async getInitialProps(ctx: DocumentContext) {
    const renderer = getFelaRenderer()
    const originalRenderPage = ctx.renderPage

    ctx.renderPage = () =>
      originalRenderPage({
        // eslint-disable-next-line react/display-name
        enhanceApp: (App) => (props) => <App {...{ ...props, renderer }} />,
      })

    const initialProps = await Document.getInitialProps(ctx)
    const styles = renderToNodeList(renderer)
    return {
      ...initialProps,
      styles: [
        ...(initialProps.styles as ReactElement<
          any,
          string | JSXElementConstructor<any>
        >[]),
        ...styles,
      ],
    }
  }

  render() {
    return (
      <Html>
        <Head />
        <body>
          <Main />
          <NextScript />
        </body>
      </Html>
    )
  }
}
