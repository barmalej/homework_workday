import { Box, Button } from '@workday-homework/components'
import { useDispatch } from 'react-redux'
import { failedToFetch } from '../lib/redux/actions'

const Error = () => {
  const dispatch = useDispatch()

  return (
    <Box
      style={{
        height: '100%',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'column',
      }}
    >
      <Box as="h1">Oppps! Somthing went wrong!</Box>
      <Box style={{ width: '100px', height: '50px' }}>
        <Button
          style={{ marginTop: '50px' }}
          onClick={(e) => {
            dispatch(failedToFetch(null))
          }}
        >
          Go back
        </Button>
      </Box>
    </Box>
  )
}

export default Error
