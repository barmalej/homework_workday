import { Box } from '@workday-homework/components'
import { useSelector } from 'react-redux'
import ControlBar from '../components/index/control-bar'
import Rates from '../components/index/rates'
import { AppState } from '../lib/types'
import Error from './_error'

const Index = () => {
  const error = useSelector((state: AppState) => state.error)

  if (error) return <Error />

  return (
    <Box>
      <ControlBar />
      <Rates />
    </Box>
  )
}

export default Index
