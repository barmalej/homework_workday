import React from 'react'
import { initializeStore, initialState } from '../lib/redux'
import Index from '../pages/index'
import { testRendererRedux } from '@workday-homework/fela/test-renderer'

describe('Index', () => {
  it('should render successfully', () => {
    const { baseElement } = testRendererRedux(
      <Index />,
      initializeStore(initialState)
    )
    expect(baseElement).toBeTruthy()
  })
})
