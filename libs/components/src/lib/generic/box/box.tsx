import { IStyle } from 'fela'
import React, { useContext } from 'react'
import { RendererContext } from 'react-fela'

export const Box = ({
  as = 'div',
  style,
  ...props
}: {
  as?: HTMLElement['tagName']
  style?: IStyle
  [x: string]: any
}) => {
  const renderer = useContext(RendererContext)
  const className = renderer.renderRule(() => (style ? style : {}), {})
  const element = React.createElement(as, {
    className,
    ...props,
  })

  return element
}
