import { testRenderer } from '@workday-homework/fela/test-renderer'
import { screen, fireEvent } from '@testing-library/react'
import { Button } from './button'

const BUTTON_TEXT = 'Button text'

describe('Button', () => {
  it('should render successfully', () => {
    const { baseElement } = testRenderer(<Button />)
    expect(baseElement).toBeTruthy()
  })

  it('text should render', () => {
    testRenderer(<Button>{BUTTON_TEXT}</Button>)
    const result = screen.getByText(BUTTON_TEXT)
    expect(result.innerHTML).toBe(BUTTON_TEXT)
  })

  it('should handle click', () => {
    const onClick = jest.fn()
    testRenderer(<Button onClick={onClick}>{BUTTON_TEXT}</Button>)
    fireEvent.click(screen.getByText(BUTTON_TEXT))
    expect(onClick).toHaveBeenCalledTimes(1)
  })
})
