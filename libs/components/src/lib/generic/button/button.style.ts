import { colors, shiftColor } from '@workday-homework/utils'
import { IStyle } from 'fela'

const buttonStyle: ({ primary }: { primary: boolean }) => IStyle = ({
  primary,
}) => {
  const color = primary ? colors.purpleHeart : colors.shattleGrey

  return {
    height: '100%',
    width: '100%',
    color: 'white',
    boxSizing: 'border-box',
    fontSize: '18px',
    backgroundColor: color,
    borderRadius: 0,
    borderWidth: '1px',
    borderStyle: 'solid',
    borderColor: 'white',
    ':hover': {
      borderColor: shiftColor('#ffffff', -5),
      color: shiftColor('#ffffff', -5),
      backgroundColor: shiftColor(color, 5),
    },
    cursor: 'pointer',
  }
}

export default buttonStyle
