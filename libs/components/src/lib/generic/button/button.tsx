import { Box } from '@workday-homework/components'
import { IStyle } from 'fela'
import { ChangeEvent, FunctionComponent } from 'react'
import buttonStyle from './button.style'

interface IProps {
  primary?: boolean
  onClick?: (e: ChangeEvent) => void
  style?: IStyle
}

export const Button: FunctionComponent<IProps> = ({
  primary = false,
  onClick,
  children,
  style,
  ...otherProps
}) => (
  <Box
    as="button"
    style={
      style
        ? { ...style, ...buttonStyle({ primary }) }
        : buttonStyle({ primary })
    }
    onClick={onClick}
    {...otherProps}
  >
    {children}
  </Box>
)
