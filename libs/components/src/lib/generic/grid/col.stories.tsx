import { Story, Meta } from '@storybook/react'
import { Col } from './col'

export default {
  component: Col,
  title: 'Col',
} as Meta

const Template: Story = (args) => <Col {...args} />

export const Primary = Template.bind({})
Primary.args = {}
