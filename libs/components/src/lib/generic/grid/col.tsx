import { IStyle } from 'fela'
import { FunctionComponent } from 'react'
import { Box } from '../box/box'

interface IProps {
  style?: IStyle
  xs?: number
  xsStart?: number
  sm?: number
  smStart?: number
  md?: number
  mdStart?: number
  lg?: number
  lgStart?: number
}

export const Col: FunctionComponent<IProps> = ({
  children,
  style = {} as IProps['style'],
  xs = 12,
  xsStart = 0,
  sm = xs,
  smStart = xsStart,
  md = sm,
  mdStart = smStart,
  lg = md,
  lgStart = mdStart,
}) => (
  <Box
    style={{
      flexShrink: 0,
      gridColumnStart: `${xsStart}`,
      gridColumnEnd: `span ${xs}`,
      minHeight: 0,
      minWidth: 0,

      maxWidthXsMax: {
        gridColumnStart: `${xsStart}`,
        gridColumnEnd: `span ${xs}`,
      },

      maxWidthSmMax: {
        minWidthSm: {
          gridColumnStart: `${smStart}`,
          gridColumnEnd: `span ${sm}`,
        },
      },

      maxWidthMdMax: {
        minWidthMd: {
          gridColumnStart: `${mdStart}`,
          gridColumnEnd: `span ${md}`,
        },
      },

      minWidthLg: {
        gridColumnStart: `${lgStart}`,
        gridColumnEnd: `span ${lg}`,
      },
      boxSizing: 'border-box',

      ...style,
    }}
  >
    {children}
  </Box>
)
