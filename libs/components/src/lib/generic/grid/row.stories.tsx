import { Story, Meta } from '@storybook/react'
import { Row } from './row'

export default {
  component: Row,
  title: 'Row',
} as Meta

const Template: Story = (args) => <Row {...args} />

export const Primary = Template.bind({})
Primary.args = {}
