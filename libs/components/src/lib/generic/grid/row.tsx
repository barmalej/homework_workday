import { IStyle } from 'fela'
import { FunctionComponent } from 'react'
import { Box } from '../box/box'

interface IProps {
  steps?: number
  gap?: number | string
  style?: IStyle
}

export const Row: FunctionComponent<IProps> = ({
  steps = 12,
  gap = '10px',
  children,
  style,
}) => (
  <Box
    style={{
      flexShrink: 0,
      display: 'grid',
      width: '100%',
      gridTemplateColumns: `repeat(${steps}, 1fr)`,
      columnGap: gap,
      boxSizing: 'border-box',
      ...style,
    }}
  >
    {children}
  </Box>
)
