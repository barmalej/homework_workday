import { Story, Meta } from '@storybook/react'
import { PageContainer } from './page-container'

export default {
  component: PageContainer,
  title: 'PageContainer',
} as Meta

const Template: Story = (args) => <PageContainer {...args} />

export const Primary = Template.bind({})
Primary.args = {}
