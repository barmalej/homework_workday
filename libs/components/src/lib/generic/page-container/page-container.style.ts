import { IStyle } from 'fela'
import { colors } from '@workday-homework/utils'

const pageContainerStyle: IStyle = {
  margin: '0 auto',
  maxWidth: '800px',
  height: '100%',
  backgroundColor: colors.pickledBluewood,
  color: '#fff',
  display: 'flex',
  flexDirection: 'column',
}

export default pageContainerStyle
