import { FunctionComponent } from 'react'
import { Box } from '../box/box'
import pageContainerStyle from './page-container.style'

export const PageContainer: FunctionComponent = ({ children }) => (
  <Box style={pageContainerStyle}>{children}</Box>
)
