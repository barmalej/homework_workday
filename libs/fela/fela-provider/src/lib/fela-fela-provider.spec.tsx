import { render } from '@testing-library/react'

import FelaFelaProvider from './fela-fela-provider'

describe('FelaFelaProvider', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<FelaFelaProvider />)
    expect(baseElement).toBeTruthy()
  })
})
