import { createRenderer } from 'fela'
import webPreset from 'fela-preset-web'
import pseudoClass from 'fela-plugin-friendly-pseudo-class'
import { globalDefaultStyle } from '@workday-homework/utils'
import namedKeysPlugin from './fela-named-keys'

export default function getRenderer() {
  const renderer = createRenderer({
    plugins: [...webPreset, pseudoClass(), namedKeysPlugin],
  })
  renderer.renderStatic(globalDefaultStyle)

  return renderer
}
