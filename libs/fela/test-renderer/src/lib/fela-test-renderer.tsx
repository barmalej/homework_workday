import { render, RenderResult } from '@testing-library/react'
import FelaProvider from '@workday-homework/fela/fela-provider'
import getFelaRenderer from '@workday-homework/fela/get-fela-renderer'
import { Provider } from 'react-redux'
import { Store } from 'redux'

export function testRendererRedux<T>(
  children: any,
  store: Store<T>
): RenderResult {
  return render(
    <FelaProvider renderer={getFelaRenderer()}>
      <Provider store={store}>{children}</Provider>
    </FelaProvider>
  )
}

export function testRenderer<T>(children: any): RenderResult {
  return render(
    <FelaProvider renderer={getFelaRenderer()}>{children}</FelaProvider>
  )
}
