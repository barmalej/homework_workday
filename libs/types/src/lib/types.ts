export interface Rates {
  date: string
  base: string
  rates: Record<string, number>
}

export interface SelectOption {
  label: string
  value: string
}

export interface ScreenSizeTypes {
  screenXxs: number
  screenXxsMin: number
  screenXs: number
  screenXsMin: number
  screenSm: number
  screenSmMin: number
  screenMd: number
  screenMdMin: number
  screenLg: number
  screenLgMin: number
  screenXxsMax: number
  screenXsMax: number
  screenSmMax: number
  screenMdMax: number
  screenXLMin: number
  screenXLMax: number
}
