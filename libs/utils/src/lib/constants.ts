import { ScreenSizeTypes } from '@workday-homework/types'

export const colors = {
  shattleGrey: '#576a79',
  pickledBluewood: '#31475b',
  silver: '#bbbbbb',
  purpleHeart: '#e3741e',
}

export const globalDefaultStyle = /*css*/ `
* {
  margin: 0;
  padding: 0
}

body, html {
  width: 100%;
  height: 100%;
  font-family: SF UI Text,Roboto,Helvetica Neue,Helvetica,sans-serif;
  background-color: #31475b;
}

body #__next {
  height: 100%;
}

hr {
  border: 0;
  height: 1px;
  background: #333;
  background-image: linear-gradient(to right, ${colors.pickledBluewood}, ${colors.shattleGrey}, ${colors.pickledBluewood});
}

.line-shine-15 {
  height: 15px;
  background-image: linear-gradient(90deg, #ccc 0px, #e8e8e8 40px, #ccc 80px);
  animation: shine-lines 1.6s infinite linear;
  background-size: 600px;
}

.line-shine-10 {
  height: 10px;
  background-image: linear-gradient(90deg, #ddd 0px, #e8e8e8 40px, #ddd 80px);
  animation: shine-lines 1.6s infinite linear;
}

@keyframes shine-lines {
  0% {
    background-position: -100px;
  }
  40%, 100% {
    background-position: 140px;
  }
}

/* The container */
.checkbox-container {
  display: block;
  position: relative;
  padding-left: 35px;
  margin-bottom: 12px;
  cursor: pointer;
  font-size: 22px;
  -webkit-user-select: none;
  -moz-user-select: none;
  -ms-user-select: none;
  user-select: none;
}

/* Hide the browser's default checkbox */
.checkbox-container input {
  position: absolute;
  opacity: 0;
  cursor: pointer;
  height: 0;
  width: 0;
}

/* Create a custom checkbox */
.checkbox-checkmark {
  position: absolute;
  top: 0;
  left: 0;
  height: 25px;
  width: 25px;
  background-color: #eee;
}

/* On mouse-over, add a grey background color */
.checkbox-container:hover input ~ .checkbox-checkmark {
  background-color: #ccc;
}

/* When the checkbox is checked, add a blue background */
.checkbox-container input:checked ~ .checkbox-checkmark {
  background-color: #2196F3;
}

/* Create the checkmark/indicator (hidden when not checked) */
.checkbox-checkmark:after {
  content: "";
  position: absolute;
  display: none;
}

/* Show the checkmark when checked */
.checkbox-container input:checked ~ .checkbox-checkmark:after {
  display: block;
}

/* Style the checkmark/indicator */
.checkbox-container .checkbox-checkmark:after {
  left: 9px;
  top: 5px;
  width: 5px;
  height: 10px;
  border: solid white;
  border-width: 0 3px 3px 0;
  -webkit-transform: rotate(45deg);
  -ms-transform: rotate(45deg);
  transform: rotate(45deg);
}
`

export const currencies = [
  'EUR',
  'USD',
  'JPY',
  'BGN',
  'CZK',
  'DKK',
  'GBP',
  'HUF',
  'PLN',
  'RON',
  'SEK',
  'CHF',
  'ISK',
  'NOK',
  'HRK',
  'RUB',
  'TRY',
  'AUD',
  'BRL',
  'CAD',
  'CNY',
  'HKD',
  'IDR',
  'ILS',
  'INR',
  'KRW',
  'MXN',
  'MYR',
  'NZD',
  'PHP',
  'SGD',
  'THB',
  'ZAR',
]

export const currenciesOptions = currencies.map((value) => ({
  label: value,
  value,
}))

export const API_URL = 'https://api.vatcomply.com/rates'

const screenXxs = 320
const screenXxsMin = screenXxs
const screenXs = 480
const screenXsMin = screenXs
const screenSm = 768
const screenSmMin = screenSm
const screenMd = 992
const screenMdMin = screenMd
const screenLg = 1260
const screenLgMin = screenLg
const screenXL = 1281
const screenXLMin = screenXL

const screenXxsMax = screenXs - 1
const screenXsMax = screenSmMin - 1
const screenSmMax = screenMdMin - 1
const screenMdMax = screenLgMin - 1
const screenXLMax = screenXLMin - 1

export const screenSizes: ScreenSizeTypes = {
  screenXxs,
  screenXxsMin,
  screenXs,
  screenXsMin,
  screenSm,
  screenSmMin,
  screenMd,
  screenMdMin,
  screenLg,
  screenLgMin,
  screenXxsMax,
  screenXsMax,
  screenSmMax,
  screenMdMax,
  screenXLMin,
  screenXLMax,
}
