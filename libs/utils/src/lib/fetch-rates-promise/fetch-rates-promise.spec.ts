import { API_URL } from '../constants'
import { fetchRatesPromise } from '.'
import { TEST_BASE_CURRENCY } from '../mock-data'

const mockFetch = () => {
  global.fetch = jest.fn(() =>
    Promise.resolve({ json: jest.fn() } as unknown as Response)
  )
}

test.each([
  // base value is presented
  ['USD', `${API_URL}?base=${TEST_BASE_CURRENCY.value}`],
  // nobase value
  [undefined, `${API_URL}`],
])(
  'fetch rates promise with base currency enqual to %s',
  (baseCurrency, testValue) => {
    mockFetch()
    fetchRatesPromise(baseCurrency)
    expect(global.fetch).toHaveBeenCalledTimes(1)
    expect(global.fetch).toHaveBeenCalledWith(testValue)
  }
)
