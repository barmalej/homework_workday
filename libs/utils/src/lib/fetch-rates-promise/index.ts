import { Rates } from '@workday-homework/types'
import { API_URL } from '../constants'

export const fetchRatesPromise = (value?: string) => {
  const url = `${API_URL}${value ? `?base=${value}` : ''}`

  return fetch(url).then((d) => d.json()) as Promise<Rates>
}
