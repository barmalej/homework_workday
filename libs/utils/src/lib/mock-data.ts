import { Rates, SelectOption } from '@workday-homework/types'

export const TEST_RATES = {} as Rates
export const TEST_BASE_CURRENCY: SelectOption = { label: '$', value: 'USD' }
export const TEST_ERROR = 'TEST ERROR'
