import { shiftColor } from '.'

test.each([
  ['#eeeeee', -20, '#dadada'],
  ['#eeeeee', 5, '#f3f3f3'],
])('shift color %s to %s and get %s', (color, shift, testValue) => {
  expect(shiftColor(color, shift)).toBe(testValue)
})
